# Add the GPG key for the official Docker repository to the system
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Add the Docker repository to APT sources
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update

# Install Docker
sudo apt-get install -y docker-ce

# Avoid Sudo with docker command
sudo usermod -aG docker ${USER}
su - ${USER}

# Get Docker Compose
sudo curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose

# Apply permission
sudo chmod +x /usr/local/bin/docker-compose

# Verify the version
docker-compose --version